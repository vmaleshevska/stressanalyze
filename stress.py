import argparse
import random
import time
from datetime import datetime, timedelta


class StressProcess:
    def __init__(self, stress_duration):
        self.stress_duration = stress_duration

    def run_stress_test(self):
        """
        Run a stress test simulation for the specified stress_duration.
        Generate 'throughput' and 'latency' randomly each second.
        """
        start_time = datetime.now()
        end_time = start_time + timedelta(seconds=self.stress_duration)
        print("Throughput (ops/s) | Latency (ms)")
        while datetime.now() < end_time:
            throughput = random.randint(0, 100000)
            latency = random.randint(0, 20000)

            # It prints the generated throughput and latency values in the specified format,
            # separated by a pipe symbol ('|').
            # The flush=True parameter ensures that the output is immediately displayed in the console.
            print(f"{throughput} | {latency}", flush=True)
            time.sleep(1)

"""
It accepts a single required command-line argument, the duration of the simulation in seconds, 
and then initializes an instance of the StressProcess class, passing the duration as a parameter. 
Finally, it calls a method on this instance to execute the stress test. 
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Stress test simulation process")
    parser.add_argument("--stress_duration", type=int, required=True,
                        help="Duration of the stress simulation in seconds")
    args = parser.parse_args()

    stress_process = StressProcess(args.stress_duration)
    stress_process.run_stress_test()
