Prerequisites:

1. Install prettytable
pip install -r requirements.txt

Run the scripts:
1. python3 stress.py --stress_duration 10
2. python3 analyze.py --threads=5
