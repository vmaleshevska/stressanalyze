import argparse
import datetime
import itertools
import random
import subprocess
import threading
from statistics import mean, quantiles

from prettytable import PrettyTable


class AnalyzeProcess:
    def __init__(self, threads: int):
        self.metrics_to_analyze = ["throughput", "latency"]
        self.threads = threads
        self.all_process_data = list()

    def run_stress_process(self, stress_duration: int):
        """
        Running a stress test process. It takes the stress test duration as input,
        runs an external script (stress.py) with the specified duration, and collects data from the process
        :param stress_duration: How long should the stress test process simulate
        """
        command = f"python3 stress.py --stress_duration={stress_duration}"
        command_parts = command.split()
        process = subprocess.Popen(command_parts, stdout=subprocess.PIPE, universal_newlines=True)
        self.collect_stress_process_data(process, stress_duration)

    def run_stress_tests_in_parallel(self):
        """
        Spawns multiple threads to run stress test processes in parallel.
        Each thread runs a stress test with a random duration, and then all threads are joined.
        """
        threads = []
        for i in range(self.threads):
            # Every thread will run in different duration between 1 and 5 seconds
            stress_duration = random.randint(1, 5)
            thread = threading.Thread(target=self.run_stress_process, args=(stress_duration,))
            thread.start()
            threads.append(thread)

        for thread in threads:
            thread.join()

        print(self.all_process_data, "\n")
        self.calculate_and_print_process_analysis()

    def calculate_and_print_process_analysis(self):
        """
        Calculates and prints statistics based on the collected data.
        It displays the number of stress processes, start times, end times, durations, and statistics for metrics
        like throughput and latency.
        """
        print(f"Number of stress processes: {len(self.all_process_data)} \n")

        process_table = PrettyTable()
        process_table.field_names = ["Process ID", "Start Time", "End Time", "Duration"]
        for i, process in enumerate(self.all_process_data):
            process_table.add_row([i, process['start_time'], process['end_time'], process['duration']])
        print(process_table)

        metric_table = PrettyTable()
        metric_table.field_names = ["Metric", "Average", "Min", "Max", "95th Percentile"]
        for metric in self.metrics_to_analyze:
            all_metric_data = self.combine_metric_data(metric)
            metric_table.add_row([
                metric,
                int(mean(all_metric_data)),
                min(all_metric_data),
                max(all_metric_data),
                quantiles(all_metric_data, n=100)[94]
            ])
        print(metric_table)

    def collect_stress_process_data(self, process, duration: int):
        """
        Collect the required data from the specific stress test
        process and add it to all_process_data

        process_data structure:
            {
                'start_time': str
                'end_time': str
                'duration': int
                'throughput': list[int]
                'latency': list[int]
            }
        :param process: the process for which to collect the data
        """
        process_data = dict()
        process_data["start_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        process_data["duration"] = duration

        for metric in self.metrics_to_analyze:
            process_data[metric] = list()

        while process.poll() is None:
            output = process.stdout.readline()
            if not output:
                break
            print(output)
            if "Throughput (ops/s) | Latency (ms)" not in output:
                throughput = int(output.split("|")[0])
                latency = int(output.split("|")[1])
                process_data["throughput"].append(throughput)
                process_data["latency"].append(latency)

        process_data["end_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.all_process_data.append(process_data)

    def combine_metric_data(self, metric: str) -> list[int]:
        """
        Combine metric (throughput, latency, etc.) data from all threads into a single list
        """
        all_metric_data = [data[metric] for data in self.all_process_data]
        return list(itertools.chain.from_iterable(all_metric_data))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Stress test results runner and analyzer")
    parser.add_argument("--threads", type=int, required=True,
                        help="How many stress test threads to run in parallel")
    args = parser.parse_args()

    stress_process = AnalyzeProcess(args.threads)
    stress_process.run_stress_tests_in_parallel()
